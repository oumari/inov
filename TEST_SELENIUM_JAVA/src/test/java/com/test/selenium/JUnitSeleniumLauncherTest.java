package com.test.selenium;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import org.junit.FixMethodOrder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;



import com.test.selenium.utils.Manager;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;


@Tag("fast")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JUnitSeleniumLauncherTest {

	private static final Logger LOG = LogManager.getLogger(JUnitSeleniumLauncherTest.class);

	private static ChromeDriverService service;
	private static WebDriver driver;
	private static String driverPath = "";
	private static String url =  "";
    private static String screen="test.png";

	
	@BeforeAll
	public static void setUpForAll(){
		LOG.info("Starting configuration ...");
		Manager.loadConfigurationProperties();	
	}

	@BeforeAll
	public static void createAndStartService() {
		//1 - Utiliser le fichier de configuration afin de rendre les éléments paramétrables
		File file = new File("src\\test\\resources\\config.properties");
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		driverPath = prop.getProperty("driverPath");
		url = prop.getProperty("url");

		
		
		LOG.info("Starting driver service ...");
		service = new ChromeDriverService.Builder()
				.usingDriverExecutable(new File(driverPath))
				.usingAnyFreePort()
				.build();
		
		try {
			service.start();
   		    driver = new RemoteWebDriver(service.getUrl(),
				DesiredCapabilities.chrome());

   		    // maximize screen
			driver.manage().window().maximize();
			
			//implicit wait=30 instead of sleep
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


		} catch (Exception e) {
			LOG.error("Error while starting driver service: " + e.getMessage());
		}

	

	}

	
	@BeforeEach
	public void createDriver() {
		// 1- Accès au site
		driver.get(url);
	}
	@AfterEach
	public void screenShot() {
		
		try {
		    //ScreenShot
			Manager.screenShot(driver, screen) ;
	    } catch (Exception e) {
	    	LOG.error("Error ScreenShot: " + e.getMessage());
	     }
		
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			LOG.error("Waiting was interrupted");
		}

	}

	@AfterAll
	public static void quitDriver() {
		driver.quit();
	}

	
	//1er Test
	@Test
	public void test1() {
		screen="test1.png";
		//2- Cliquer sur la barre de recherche 
		WebElement searchButton = driver.findElement(By.xpath("//button[contains(@data-test,'btnSearch_Header_Disabled')]"));
		searchButton.click();
		
		//3- Chercher le mot "ROUGE ALLURE"
		WebElement searchInput = driver.findElement(By.id("searchInput"));		
		searchInput.sendKeys("ROUGE ALLURE");
		
		// Search
		WebElement btnSubmitSearch = driver.findElement(By.xpath("//button[contains(@data-test,'btnSubmitSearch')]"));
		btnSubmitSearch.click();
		

		// Récupérer le deuxième élément de la liste
		WebElement productListInline = driver.findElements(By.className("product-list-inline")).get(1);
		String productListInlineTitle = productListInline.findElements(By.tagName("h4")).get(0).getText().toUpperCase();

        //3- Vérifier si l'élément affiché contient le mot recherché ("ROUGE ALLURE")
	    assertTrue(productListInlineTitle.contains("ROUGE ALLURE"));


    	//4- Séléctionner le deuxième élément de la liste
		productListInline.click();
		
		// Récupérer le nom du produit affiché		
		String lblProductTitle = driver.findElement(By.xpath("//span[contains(@data-test,'lblProductTitle')]")).getText().toUpperCase();
		
		
		//5- S'assurer que le nom du produit affiché et le même que celui sélectionné auparavant
		assertEquals(productListInlineTitle,lblProductTitle);

		System.out.print("TEST 1 TERMINE");

	}
	
	
	//2em Test
	@Test
	public void test2() {
		screen="test2.png";
		// Accept Cookies
		WebElement cookie = driver.findElement(By.id("onetrust-accept-btn-handler"));		
		cookie.click();

		
		//2- Accès à la catégorie Makeup 
		WebElement makeupNav = driver.findElement(By.id("makeup"));
		makeupNav.click();

		// clique > Lipstick 
		driver.findElement(By.linkText("Lipstick")).click();
		
        //3-clique sur le produit "ROUGE ALLURE"
		WebElement product = driver.findElement(By.xpath("//*[text()='ROUGE ALLURE']"));
		product.click();

		//Récupérer le prix du produit
		String lblProductPrice = driver.findElement(By.xpath("//p[contains(@data-test,'lblProductPrice_EditProduct')]")).getText();
		
        //4- Ajouter le produit au panier
		WebElement addBag = driver.findElement(By.id("pos-cnc-btn"));
		addBag.click();

		//Récupérer la quantité du produit		
		String productQuantity = driver.findElement(By.className("cart-product__quantity")).getText();

		//Récupérer le prix total du produit
		String lblSubTotalPrice = driver.findElement(By.xpath("//p[contains(@data-test,'lblSubTotal_Price')]")).getText();

        //Ex: $4.OO et $4 unifier le format au 4.0 >> considerant que le devise est toujours $
		Double lblProductPriceValue= Double.parseDouble(lblProductPrice.replace("$", ""));
		Double lblSubTotalPriceValue=Double.parseDouble(lblSubTotalPrice.replace("$", ""));

		String productTitle = driver.findElement(By.xpath("//a[contains(@data-test,'secProductName_0')]//span")).getText();

		
		//5- Si Produit "ROUGE ALLURE" est dans le panier
		assertEquals(productTitle.toUpperCase(),"ROUGE ALLURE");

		//5- Si le total panier est égal au prix du produit
		assertEquals(lblProductPriceValue,lblSubTotalPriceValue);

		//5- Si la quantité =1
		assertEquals(productQuantity,"QTY 1");

		//5- Afficher le panier
		WebElement btnReviewBag = driver.findElement(By.xpath("//a[contains(@data-test,'btnReviewBag')]"));
		btnReviewBag.click();
		
		System.out.print("TEST 2 TERMINE");

	}
}
